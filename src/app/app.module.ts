import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { NgxsModule } from '@ngxs/store';
import { TaskListState } from './store/state/todo.state';
import { PendingTasksComponent } from './components/pending-tasks/pending-tasks.component';
import { ArchivedTasksComponent } from './components/archived-tasks/archived-tasks.component';
import { DoneTasksComponent } from './components/done-tasks/done-tasks.component';
import { AppRoutingModule } from './app-routing.module';
import { AppMaterialModule } from './app-material.module';
import { TaskItemComponent } from './components/task-item/task-item.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { FormsModule } from '@angular/forms';

@NgModule({
  declarations: [
    AppComponent,
    PendingTasksComponent,
    ArchivedTasksComponent,
    DoneTasksComponent,
    TaskItemComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    FormsModule,
    AppRoutingModule,
    AppMaterialModule,
    NgxsModule.forRoot([
      TaskListState
    ])
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule {}

import { Component, ElementRef, ViewChild } from '@angular/core';
import { Select, Store } from '@ngxs/store';
import { CreateTask } from '../../store/actions/create-task.action';
import { Observable } from 'rxjs/index';
import { Task } from '../../store/models/task.model';
import { TaskListState } from '../../store/state/todo.state';

@Component({
  selector: 'app-pending-tasks',
  templateUrl: './pending-tasks.component.html',
  styleUrls: ['./pending-tasks.component.scss']
})
export class PendingTasksComponent {
  @Select(TaskListState.pendings) tasksObs: Observable<Task[]>;

  constructor(private store: Store) { }

  addTask(input) {
    this.store.dispatch(new CreateTask(input.value)).subscribe(() => {});
    input.value = '';
  }
}

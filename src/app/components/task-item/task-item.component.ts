import { Component, Input, OnInit } from '@angular/core';
import { Task } from '../../store/models/task.model';
import { Store } from '@ngxs/store';
import { DoneTask } from '../../store/actions/done-task.action';
import { ArchiveTask } from '../../store/actions/archive-task.action';
import { DeleteTask } from '../../store/actions/delete-task.action';

@Component({
  selector: 'app-task-item',
  templateUrl: './task-item.component.html',
  styleUrls: ['./task-item.component.scss']
})
export class TaskItemComponent implements OnInit {
  @Input() task: Task;

  constructor(private store: Store) { }

  ngOnInit() {
  }

  changeDone() {
    this.store.dispatch(new DoneTask(this.task));
  }

  archiveTask() {
    this.store.dispatch(new ArchiveTask(this.task));
  }

  deleteTask() {
    this.store.dispatch(new DeleteTask(this.task.taskId));
  }
}

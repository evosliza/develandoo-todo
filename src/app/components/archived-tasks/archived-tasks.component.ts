import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs/index';
import { Select } from '@ngxs/store';
import { Task } from '../../store/models/task.model';
import { TaskListState } from '../../store/state/todo.state';

@Component({
  selector: 'app-archived-tasks',
  templateUrl: './archived-tasks.component.html',
  styleUrls: ['./archived-tasks.component.scss']
})
export class ArchivedTasksComponent implements OnInit {
  @Select(TaskListState.archived) tasksObs: Observable<Task[]>;

  constructor() { }

  ngOnInit() {
  }

}

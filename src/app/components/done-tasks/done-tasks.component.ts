import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs/index';
import { Select } from '@ngxs/store';
import { Task } from '../../store/models/task.model';
import { TaskListState } from '../../store/state/todo.state';

@Component({
  selector: 'app-done-tasks',
  templateUrl: './done-tasks.component.html',
  styleUrls: ['./done-tasks.component.scss']
})
export class DoneTasksComponent implements OnInit {
  @Select(TaskListState.done) tasksObs: Observable<Task[]>;

  constructor() { }

  ngOnInit() {
  }

}

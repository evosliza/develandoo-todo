import { Injectable } from '@angular/core';
import { of } from 'rxjs/index';
import { Task } from '../store/models/task.model';

const TASK_LIST_KEY = 'task-list-key';

@Injectable({
  providedIn: 'root'
})
export class TaskService {

  getTasks() {
    const tasks = this.getTasksFromStorage();
    return of(tasks);
  }

  createTask(task: Task) {
    const tasks = this.getTasksFromStorage();
    tasks.push(task);
    this.saveTasksToStorage(tasks);
    return of(task);
  }

  updateTask(task: Task) {
    const tasks = this.getTasksFromStorage();
    const selectedTask = tasks.find((t) => t.taskId === task.taskId);
    Object.assign(selectedTask, task);
    this.saveTasksToStorage(tasks);
    return of(selectedTask);
  }

  deleteTask(taskId: string) {
    const tasks = this.getTasksFromStorage();
    const index = tasks.findIndex((t) => t.taskId === taskId);
    if(index >= 0) {
      tasks.splice(index, 1);
      this.saveTasksToStorage(tasks);
    }
    return of(null);
  }

  private getTasksFromStorage(): Task[] {
    const tasksJson = localStorage.getItem(TASK_LIST_KEY);
    return tasksJson ? JSON.parse(tasksJson) : [];
  }

  private saveTasksToStorage(tasks: Task[]) {
    localStorage.setItem(TASK_LIST_KEY, JSON.stringify(tasks));
  }
}

import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { PendingTasksComponent } from './components/pending-tasks/pending-tasks.component';
import { ArchivedTasksComponent } from './components/archived-tasks/archived-tasks.component';
import { DoneTasksComponent } from './components/done-tasks/done-tasks.component';

const routes: Routes = [
  { path: '', pathMatch: 'full', component: PendingTasksComponent },
  { path: 'archived', component: ArchivedTasksComponent },
  { path: 'done', component: DoneTasksComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {}

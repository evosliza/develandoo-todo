import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  tabs = [
    {name: 'Pending Tasks', path: '/'},
    {name: 'Done Tasks', path: '/done'},
    {name: 'Archived Tasks', path: '/archived'}
  ];
}

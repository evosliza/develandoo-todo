export interface Task {
  taskId: string;
  taskName: string;
  done: boolean;
  archived: boolean;
}

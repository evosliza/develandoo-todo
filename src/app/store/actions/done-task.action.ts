import { Task } from '../models/task.model';

export class DoneTask implements Task {
  static readonly type = 'done-task-action';

  taskId: string;
  taskName: string;
  done: boolean;
  archived: boolean;

  constructor(task: Task) {
    Object.assign(this, task);

    this.done = !this.done;
  }
}

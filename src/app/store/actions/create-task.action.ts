import { guid } from '../../utils/helper-methods';
import { Task } from '../models/task.model';

export class CreateTask implements Task {
  static readonly type = 'create-task-action';

  taskId = guid();
  taskName: string;
  done = false;
  archived = false;

  constructor(taskName: string) {
    this.taskName = taskName;
  }
}

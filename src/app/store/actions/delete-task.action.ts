export class DeleteTask {
  static readonly type = 'delete-task-action';

  taskId: string;

  constructor(taskId: string) {
    this.taskId = taskId;
  }
}

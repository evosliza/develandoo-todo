import { Task } from '../models/task.model';

export class ArchiveTask implements Task {
  static readonly type = 'archive-task-action';

  taskId: string;
  taskName: string;
  done: boolean;
  archived: boolean;

  constructor(task: Task) {
    Object.assign(this, task);

    this.archived = !this.archived;
  }
}

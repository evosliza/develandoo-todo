import { Task } from '../models/task.model';
import { TaskService } from '../../services/task.service';
import { Action, NgxsOnInit, Selector, State, StateContext } from '@ngxs/store';
import { CreateTask } from '../actions/create-task.action';
import { toPromise } from '../../utils/helper-methods';
import { DoneTask } from '../actions/done-task.action';
import { ArchiveTask } from '../actions/archive-task.action';
import { DeleteTask } from '../actions/delete-task.action';

@State<Task[]>({
  name: 'taskList',
  defaults: []
})
export class TaskListState implements NgxsOnInit {
  constructor(private taskService: TaskService) { }

  async ngxsOnInit(ctx: StateContext<Task[]>) {
    const tasks = await toPromise(this.taskService.getTasks());
    ctx.setState(tasks);
  }

  @Action(CreateTask)
  async createTask(ctx: StateContext<Task[]>, action: CreateTask) {
    const tasks = ctx.getState();

    const task = await toPromise(this.taskService.createTask(action));
    ctx.setState([
      ...tasks,
      task
    ]);
  }


  @Action(DoneTask)
  async doneTask(ctx: StateContext<Task[]>, action: DoneTask) {
    const tasks = ctx.getState();

    const task = await toPromise(this.taskService.updateTask(action));

    const selectedTask = tasks.find((t) => t.taskId === task.taskId);
    Object.assign(selectedTask, task);

    ctx.setState([...tasks]);
  }

  @Action(ArchiveTask)
  async archiveTask(ctx: StateContext<Task[]>, action: ArchiveTask) {
    const tasks = ctx.getState();

    const task = await toPromise(this.taskService.updateTask(action));

    const selectedTask = tasks.find((t) => t.taskId === task.taskId);
    Object.assign(selectedTask, task);

    ctx.setState([...tasks]);
  }

  @Action(DeleteTask)
  async deleteTask(ctx: StateContext<Task[]>, action: DeleteTask) {
    const tasks = ctx.getState();

    await toPromise(this.taskService.deleteTask(action.taskId));

    const index = tasks.findIndex((t) => t.taskId === action.taskId);
    if(index >= 0) {
      tasks.splice(index, 1);
      ctx.setState([...tasks]);
    }
  }

  @Selector()
  static pendings(tasks: Task[]) {
    return tasks.filter(task => !task.done && !task.archived);
  }

  @Selector()
  static done(tasks: Task[]) {
    return tasks.filter(task => task.done && !task.archived);
  }

  @Selector()
  static archived(tasks: Task[]) {
    return tasks.filter(task => task.archived);
  }
}

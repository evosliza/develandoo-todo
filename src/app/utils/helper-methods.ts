import { Observable } from 'rxjs/index';

const rand16 = (length = Infinity) => {
  return Math.random().toString(16).substr(2, length);
};
export const guid = () => {
  return `${rand16(8)}-${rand16(4)}-${rand16(4)}-${rand16(4)}-${rand16(12)}`;
};

export const toPromise = <T>(obs: Observable<T>) => {
  return new Promise<T>((resolve, reject) => {
    obs.subscribe({
      next: resolve,
      error: reject
    });
  });
};
